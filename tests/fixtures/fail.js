/**
 *
 * combine source maps
 * fail fixture
 *
 */


import { SourceMapGenerator } from 'source-map';

let gen0 = new SourceMapGenerator({ file: 'intermediate.js' });
let gen1 = new SourceMapGenerator({ file: 'final.js' });

let posOriginal = { line: 33, column: 2 };
let posIntermediate = { line: 10, column: 35 };
let posFinal = { line: 18, column: 16 };

gen0.addMapping({
  generated: posIntermediate,
  original: posOriginal,
  name: 'foobar',
  source: 'source.js'
});

gen1.addMapping({
  generated: posFinal,
  // here is the error
  original: { line: 42, column: 35 },
  name: 'qux',
  source: 'intermediate.js'
});

let map0 = JSON.parse(gen0.toString());
let map1 = JSON.parse(gen1.toString());

export { map0, map1, posOriginal, posFinal }
