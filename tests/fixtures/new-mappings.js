/**
 *
 * combine source maps
 * 'new mappings' fixture
 *
 */


import { SourceMapGenerator } from 'source-map';

let gen0 = new SourceMapGenerator({ file: 'intermediate.js' });
let gen1 = new SourceMapGenerator({ file: 'final.js' });

let posOriginal = { line: 33, column: 2 };
let posFinal = { line: 18, column: 16 };

gen1.addMapping({
  generated: posFinal,
  original: posOriginal,
  name: 'qux',
  source: 'intermediate.js'
});

let map0 = JSON.parse(gen0.toString());
let map1 = JSON.parse(gen1.toString());

export { map0, map1, posOriginal, posFinal }
