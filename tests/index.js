/**
 *
 * combine source maps
 * tests
 *
 */


import { deepStrictEqual, throws } from 'assert';
import { SourceMapConsumer, SourceMapGenerator } from 'source-map';

import * as fixture0 from './fixtures/default';
import * as fixture1 from './fixtures/fail';
import * as fixture2 from './fixtures/new-mappings';
import combineSourceMaps from '../index';


describe('combineSourceMaps()', () => {
  it('should combine source maps', () => {
    let rawSourceMap = JSON.parse(combineSourceMaps(fixture0.map0, fixture0.map1));
    let smc = new SourceMapConsumer(rawSourceMap);
    let pos = smc.originalPositionFor(fixture0.posFinal);

    deepStrictEqual(pos, {
      line: fixture0.posOriginal.line,
      column: fixture0.posOriginal.column,
      name: 'foobar',
      source: 'source.js'
    });
  });

  it('should fail with incompatible source maps', () => {
    throws(
      () => combineSourceMaps(fixture1.map0, fixture1.map1),
      /Alone mapping/
    );
  });

  it('shoud allow new mappings in the second source map', () => {
    let rawSourceMap = JSON.parse(combineSourceMaps(fixture2.map0, fixture2.map1));
    let smc = new SourceMapConsumer(rawSourceMap);
    let pos = smc.originalPositionFor(fixture2.posFinal);

    deepStrictEqual(pos, {
      line: fixture2.posOriginal.line,
      column: fixture2.posOriginal.column,
      name: 'qux',
      source: 'intermediate.js'
    });
  });
});
