/**
 *
 * combine source maps
 * benchmarks
 *
 */


import * as fixture from './fixtures/default';
import combineSourceMaps from '../index';


let length = 10000;
let start = process.hrtime();

for (let i = 0; i < length; i++) {
  combineSourceMaps(fixture.map0, fixture.map1);
}

let diff = process.hrtime(start);
let duration = diff[0] + diff[1] / 1e9;

console.log('%d combinations done in %ds', length, duration);
console.log('1 combination done in %dms', duration / length * 1000);
