# Combine source maps

[![build status](https://gitlab.com/slietar/combine-source-maps/badges/master/build.svg)](https://gitlab.com/slietar/combine-source-maps/builds)


## Installation

```sh
$ npm install gitlab:slietar/combine-source-maps
```


## Usage

```js
var combineSourceMaps = require('combine-source-maps');

var map0 = require('step1.js.map');
var map1 = require('step2.js.map');

var finalMap = combineSourceMaps(map0, map1);

console.log(JSON.stringify(finalMap.toString()));
```
