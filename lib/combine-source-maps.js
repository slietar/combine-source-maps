'use strict';

var sourceMap = require('source-map');

var babelHelpers = {};

babelHelpers.toConsumableArray = function (arr) {
  if (Array.isArray(arr)) {
    for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) arr2[i] = arr[i];

    return arr2;
  } else {
    return Array.from(arr);
  }
};

babelHelpers;

function combineSourceMaps() {
  for (var _len = arguments.length, sourceMaps = Array(_len), _key = 0; _key < _len; _key++) {
    sourceMaps[_key] = arguments[_key];
  }

  var sm0 = sourceMaps[0];
  var sm1 = sourceMaps[1];
  var otherSourceMaps = sourceMaps.slice(2);


  var smc0 = new sourceMap.SourceMapConsumer(sm0);
  var smc1 = new sourceMap.SourceMapConsumer(sm1);
  var map = new sourceMap.SourceMapGenerator({
    file: smc1.file
  });

  smc0.eachMapping(function (mapping) {
    var position = smc1.generatedPositionFor({
      source: sm0.file,
      line: mapping.generatedLine,
      column: mapping.generatedColumn
    });

    if (position.line === null || position.column === null) {
      throw new Error('Alone mapping at (' + mapping.generatedLine + ', ' + mapping.generatedColumn + ')');
    }

    map.addMapping({
      generated: {
        line: position.line,
        column: position.column
      },
      source: mapping.source,
      original: {
        line: mapping.originalLine,
        column: mapping.originalColumn
      },
      name: mapping.name
    });
  });

  smc1.eachMapping(function (mapping) {
    var originalPosition = smc1.originalPositionFor({
      line: mapping.originalLine,
      column: mapping.originalColumn
    });

    if (originalPosition.line !== null || originalPosition.line !== null) {
      return;
    }

    map.addMapping({
      generated: {
        line: mapping.generatedLine,
        column: mapping.generatedColumn
      },
      source: mapping.source,
      original: {
        line: mapping.originalLine,
        column: mapping.originalColumn
      },
      name: mapping.name
    });
  });

  return otherSourceMaps.length > 0 ? combineSourceMaps.apply(undefined, [map].concat(babelHelpers.toConsumableArray(otherSourceMaps))) : map;
}

module.exports = combineSourceMaps;