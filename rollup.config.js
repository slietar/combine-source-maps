import babel from 'rollup-plugin-babel';

export default {
  entry: 'index.js',
  dest: 'lib/combine-source-maps.js',
  format: 'cjs',
  external: ['assert', 'source-map'],
  plugins: [
    babel({
      presets: ['es2015-rollup']
    })
  ]
}
