/**
 *
 * combine source maps
 *
 */


import { SourceMapConsumer, SourceMapGenerator } from 'source-map';


export default function combineSourceMaps(...sourceMaps) {
  let [sm0, sm1, ...otherSourceMaps] = sourceMaps;

  let smc0 = new SourceMapConsumer(sm0);
  let smc1 = new SourceMapConsumer(sm1);
  let map = new SourceMapGenerator({
    file: smc1.file
  });

  smc0.eachMapping((mapping) => {
    let position = smc1.generatedPositionFor({
      source: sm0.file,
      line: mapping.generatedLine,
      column: mapping.generatedColumn
    });

    if (position.line === null || position.column === null) {
      throw new Error(`Alone mapping at (${mapping.generatedLine}, ${mapping.generatedColumn})`);
    }

    map.addMapping({
      generated: {
        line: position.line,
        column: position.column
      },
      source: mapping.source,
      original: {
        line: mapping.originalLine,
        column: mapping.originalColumn
      },
      name: mapping.name
    });
  });

  smc1.eachMapping((mapping) => {
    let originalPosition = smc1.originalPositionFor({
      line: mapping.originalLine,
      column: mapping.originalColumn
    });

    if (originalPosition.line !== null || originalPosition.line !== null) {
      return;
    }

    map.addMapping({
      generated: {
        line: mapping.generatedLine,
        column: mapping.generatedColumn
      },
      source: mapping.source,
      original: {
        line: mapping.originalLine,
        column: mapping.originalColumn
      },
      name: mapping.name
    });
  });

  return otherSourceMaps.length > 0
    ? combineSourceMaps(map, ...otherSourceMaps)
    : map;
}
